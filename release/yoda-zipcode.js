(function(angular) {
    'use strict';

    angular.module('yodaZipCode', [])
        .provider('yodaZipCode', yodaZipCode);

    function yodaZipCode() {
        var self = this,
            _url = null,
            _disableCache = null

        this.setUrl = function(url) {
            _url = url || '';
        }

        this.disableCache = function(answer) {
            _disableCache = answer;
        }

        this.$get = ['$injector', '$q', $get];

        function $get($resource, $q) {
            var _cache = null;

            if (!disableCache) {
                return function(zipcode) {
                    if (cache && cache.zipcode === zipcode) {
                        return $q.resolve(cache.data);
                    }

                    return search(zipcode);
                }
            }

            return search;

            function search(zipcode) {
                var defer = $q.defer();
                
                $resource(self._url).get({ zipcode: zipcode }).$promise
                    .then(function(data) {
                        _cache = {
                            zipcode: zipcode,
                            data: data
                        };

                        defer.resolve(data);
                    }).catch(function(error) {
                        defer.reject(error);
                    });

                return defer.promise;
            }
        }
    };

})(window.angular);